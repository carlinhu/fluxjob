﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 AudioController AudioController::get_Instance()
extern void AudioController_get_Instance_mED6F11E1732F5F8C02D9F001DB4F47011C1332DB (void);
// 0x00000002 System.Void AudioController::Awake()
extern void AudioController_Awake_m46F0037C36EE66B9987DE7C7FEE05248EC66457D (void);
// 0x00000003 System.Void AudioController::SetMasterVolume(System.Single)
extern void AudioController_SetMasterVolume_m24FFD1948D78C3019C36D7547693FBE25F3D24B4 (void);
// 0x00000004 System.Void AudioController::.ctor()
extern void AudioController__ctor_mFBC6DD9163DE0AC6DB9CD48A5D56326117FCDDEA (void);
// 0x00000005 System.Void Life::TakesLife(System.Int32)
extern void Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD (void);
// 0x00000006 System.Void Life::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178 (void);
// 0x00000007 System.Void Life::.ctor()
extern void Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926 (void);
// 0x00000008 System.Void ControlGame::Start()
extern void ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225 (void);
// 0x00000009 System.Void ControlGame::Update()
extern void ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50 (void);
// 0x0000000A System.Void ControlGame::LevelPassed()
extern void ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534 (void);
// 0x0000000B System.Void ControlGame::GameOver()
extern void ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13 (void);
// 0x0000000C System.Void ControlGame::Clear()
extern void ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074 (void);
// 0x0000000D System.Void ControlGame::PauseLogic()
extern void ControlGame_PauseLogic_m563DA59562EABB99F810ADC81B48820DF75F5969 (void);
// 0x0000000E System.Void ControlGame::PauseGame()
extern void ControlGame_PauseGame_m341859C0FD7778EBDA18E51802B2BDBACA7F8F16 (void);
// 0x0000000F System.Void ControlGame::ResumeGame()
extern void ControlGame_ResumeGame_mB7970A4D517A15D3AF2ABEB768BFB365B20D3A46 (void);
// 0x00000010 System.Void ControlGame::Quit()
extern void ControlGame_Quit_m4E86B42237B8B159E22DB886C98D4728917951BD (void);
// 0x00000011 System.Void ControlGame::HireDev()
extern void ControlGame_HireDev_mC60F47B42C315084299E00B0F57ADD0755A250B0 (void);
// 0x00000012 System.Void ControlGame::.ctor()
extern void ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D (void);
// 0x00000013 System.Void ControlGame::.cctor()
extern void ControlGame__cctor_m6C5D0F35594B1A5759E17D9EB7EC4B947A1B9E1F (void);
// 0x00000014 System.Void ControlStart::Start()
extern void ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51 (void);
// 0x00000015 System.Void ControlStart::StartClick()
extern void ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB (void);
// 0x00000016 System.Void ControlStart::Quit()
extern void ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747 (void);
// 0x00000017 System.Void ControlStart::.ctor()
extern void ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA (void);
// 0x00000018 System.Void Dodge::Start()
extern void Dodge_Start_mD051BAAE091535AD3F0D41FAB28C371524FF35C1 (void);
// 0x00000019 System.Void Dodge::Update()
extern void Dodge_Update_m8AFBBB092AA88131C1CF4FD90951AA6583C388FB (void);
// 0x0000001A System.Void Dodge::DangerChecker()
extern void Dodge_DangerChecker_mA729042296D9D642677101BFC792F3AE58F94FF3 (void);
// 0x0000001B System.Void Dodge::PerformDodge(UnityEngine.Vector2)
extern void Dodge_PerformDodge_m7A57CE5FD4F7D7BE417163909D44BCD1361F73D7 (void);
// 0x0000001C System.Void Dodge::OnDrawGizmos()
extern void Dodge_OnDrawGizmos_mA816507E0A3F53D71A0BFBD2E301FF08A996F6DB (void);
// 0x0000001D System.Void Dodge::.ctor()
extern void Dodge__ctor_mF2B67DAAE4FDA0BDC7962837BE577374DA570E52 (void);
// 0x0000001E System.Void EnemyControl::Start()
extern void EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F (void);
// 0x0000001F System.Collections.IEnumerator EnemyControl::Process()
extern void EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B (void);
// 0x00000020 System.Void EnemyControl::EnemyCreate()
extern void EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E (void);
// 0x00000021 System.Collections.IEnumerator EnemyControl::CallBoss()
extern void EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101 (void);
// 0x00000022 System.Void EnemyControl::.ctor()
extern void EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF (void);
// 0x00000023 System.Void EnemyControl/<Process>d__4::.ctor(System.Int32)
extern void U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15 (void);
// 0x00000024 System.Void EnemyControl/<Process>d__4::System.IDisposable.Dispose()
extern void U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2 (void);
// 0x00000025 System.Boolean EnemyControl/<Process>d__4::MoveNext()
extern void U3CProcessU3Ed__4_MoveNext_m3A08B74C2B6D96C327D949D051A8EA8D5B423DDC (void);
// 0x00000026 System.Object EnemyControl/<Process>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B (void);
// 0x00000027 System.Void EnemyControl/<Process>d__4::System.Collections.IEnumerator.Reset()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D (void);
// 0x00000028 System.Object EnemyControl/<Process>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996 (void);
// 0x00000029 System.Void EnemyControl/<CallBoss>d__6::.ctor(System.Int32)
extern void U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363 (void);
// 0x0000002A System.Void EnemyControl/<CallBoss>d__6::System.IDisposable.Dispose()
extern void U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9 (void);
// 0x0000002B System.Boolean EnemyControl/<CallBoss>d__6::MoveNext()
extern void U3CCallBossU3Ed__6_MoveNext_mDF3544E995D32004659624A65FB35CCD76C0B909 (void);
// 0x0000002C System.Object EnemyControl/<CallBoss>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9 (void);
// 0x0000002D System.Void EnemyControl/<CallBoss>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D (void);
// 0x0000002E System.Object EnemyControl/<CallBoss>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27 (void);
// 0x0000002F System.Void RecordControl::Start()
extern void RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F (void);
// 0x00000030 System.Void RecordControl::UpdateName()
extern void RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC (void);
// 0x00000031 System.Void RecordControl::.ctor()
extern void RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4 (void);
// 0x00000032 System.Void Boss2::Start()
extern void Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8 (void);
// 0x00000033 System.Collections.IEnumerator Boss2::ShowParts()
extern void Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315 (void);
// 0x00000034 System.Void Boss2::Update()
extern void Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14 (void);
// 0x00000035 System.Collections.IEnumerator Boss2::Shot()
extern void Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6 (void);
// 0x00000036 UnityEngine.GameObject Boss2::GetOneActive()
extern void Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3 (void);
// 0x00000037 System.Void Boss2::KillMe(UnityEngine.GameObject)
extern void Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285 (void);
// 0x00000038 System.Void Boss2::.ctor()
extern void Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F (void);
// 0x00000039 System.Void Boss2/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8 (void);
// 0x0000003A System.Void Boss2/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146 (void);
// 0x0000003B System.Boolean Boss2/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704 (void);
// 0x0000003C System.Object Boss2/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41 (void);
// 0x0000003D System.Void Boss2/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D (void);
// 0x0000003E System.Object Boss2/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A (void);
// 0x0000003F System.Void Boss2/<Shot>d__7::.ctor(System.Int32)
extern void U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C (void);
// 0x00000040 System.Void Boss2/<Shot>d__7::System.IDisposable.Dispose()
extern void U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7 (void);
// 0x00000041 System.Boolean Boss2/<Shot>d__7::MoveNext()
extern void U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE (void);
// 0x00000042 System.Object Boss2/<Shot>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6 (void);
// 0x00000043 System.Void Boss2/<Shot>d__7::System.Collections.IEnumerator.Reset()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62 (void);
// 0x00000044 System.Object Boss2/<Shot>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25 (void);
// 0x00000045 System.Void Boss3::Start()
extern void Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C (void);
// 0x00000046 System.Collections.IEnumerator Boss3::ShowParts(System.Boolean)
extern void Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6 (void);
// 0x00000047 System.Collections.IEnumerator Boss3::AttackNow()
extern void Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420 (void);
// 0x00000048 System.Collections.IEnumerator Boss3::Attack(UnityEngine.UI.Image)
extern void Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006 (void);
// 0x00000049 UnityEngine.GameObject Boss3::GetOneActive()
extern void Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F (void);
// 0x0000004A System.Void Boss3::KillMe(UnityEngine.GameObject)
extern void Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029 (void);
// 0x0000004B System.Void Boss3::.ctor()
extern void Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306 (void);
// 0x0000004C System.Void Boss3/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D (void);
// 0x0000004D System.Void Boss3/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962 (void);
// 0x0000004E System.Boolean Boss3/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_mCD5696204CDF2A5ECBC56EF51E055A070AC9FB77 (void);
// 0x0000004F System.Object Boss3/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F (void);
// 0x00000050 System.Void Boss3/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524 (void);
// 0x00000051 System.Object Boss3/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF (void);
// 0x00000052 System.Void Boss3/<AttackNow>d__6::.ctor(System.Int32)
extern void U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9 (void);
// 0x00000053 System.Void Boss3/<AttackNow>d__6::System.IDisposable.Dispose()
extern void U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521 (void);
// 0x00000054 System.Boolean Boss3/<AttackNow>d__6::MoveNext()
extern void U3CAttackNowU3Ed__6_MoveNext_mEA7C3F8A7C4FA8E6D4387CE9F9A6709AFEB394E7 (void);
// 0x00000055 System.Object Boss3/<AttackNow>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA (void);
// 0x00000056 System.Void Boss3/<AttackNow>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A (void);
// 0x00000057 System.Object Boss3/<AttackNow>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0 (void);
// 0x00000058 System.Void Boss3/<Attack>d__7::.ctor(System.Int32)
extern void U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327 (void);
// 0x00000059 System.Void Boss3/<Attack>d__7::System.IDisposable.Dispose()
extern void U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF (void);
// 0x0000005A System.Boolean Boss3/<Attack>d__7::MoveNext()
extern void U3CAttackU3Ed__7_MoveNext_mA361D488087872EB4D071C00B7A2E97B943ED301 (void);
// 0x0000005B System.Object Boss3/<Attack>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52 (void);
// 0x0000005C System.Void Boss3/<Attack>d__7::System.Collections.IEnumerator.Reset()
extern void U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7 (void);
// 0x0000005D System.Object Boss3/<Attack>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F (void);
// 0x0000005E System.Void Enemy::Start()
extern void Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02 (void);
// 0x0000005F System.Void Enemy::Update()
extern void Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2 (void);
// 0x00000060 System.Void Enemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12 (void);
// 0x00000061 System.Void Enemy::MyDeath()
extern void Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8 (void);
// 0x00000062 System.Collections.IEnumerator Enemy::KillMe()
extern void Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5 (void);
// 0x00000063 System.Void Enemy::Create(System.Int32)
extern void Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759 (void);
// 0x00000064 System.Collections.IEnumerator Enemy::Shoot()
extern void Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767 (void);
// 0x00000065 System.Void Enemy::DangerChecker()
extern void Enemy_DangerChecker_mDEDE63E42CFF2B4849A499F531D806C8FDEA16DA (void);
// 0x00000066 System.Void Enemy::PerformDodge(UnityEngine.Vector2)
extern void Enemy_PerformDodge_mD2926F96DBB18BAE7F936108FAC924BD05028BE8 (void);
// 0x00000067 System.Void Enemy::EndBoss()
extern void Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA (void);
// 0x00000068 System.Void Enemy::PStick()
extern void Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173 (void);
// 0x00000069 System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x0000006A System.Void Enemy/<KillMe>d__17::.ctor(System.Int32)
extern void U3CKillMeU3Ed__17__ctor_m3DA8A933BD3CC129D87F3A47FBD8C14D2E95590D (void);
// 0x0000006B System.Void Enemy/<KillMe>d__17::System.IDisposable.Dispose()
extern void U3CKillMeU3Ed__17_System_IDisposable_Dispose_mB779954E5AB9233392E43E900B2E83A2F6FF32E9 (void);
// 0x0000006C System.Boolean Enemy/<KillMe>d__17::MoveNext()
extern void U3CKillMeU3Ed__17_MoveNext_mCFD48D9A2CFE2952982C67DCF50FF9A171C10F7A (void);
// 0x0000006D System.Object Enemy/<KillMe>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKillMeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m807A3FCAA01770682E10BDF7E62BB914840C8DA1 (void);
// 0x0000006E System.Void Enemy/<KillMe>d__17::System.Collections.IEnumerator.Reset()
extern void U3CKillMeU3Ed__17_System_Collections_IEnumerator_Reset_m08CD3B59C1BE9755AE2142FCE7754A444ABEE665 (void);
// 0x0000006F System.Object Enemy/<KillMe>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CKillMeU3Ed__17_System_Collections_IEnumerator_get_Current_m6BE8FFE908ABBD42DAD6B9AC5198ACC1D874495E (void);
// 0x00000070 System.Void Enemy/<Shoot>d__19::.ctor(System.Int32)
extern void U3CShootU3Ed__19__ctor_m98BA24A73038966BD3A9243D46291DA0F80E7127 (void);
// 0x00000071 System.Void Enemy/<Shoot>d__19::System.IDisposable.Dispose()
extern void U3CShootU3Ed__19_System_IDisposable_Dispose_m896974EDA174BA23BC280639447BAB578440E27A (void);
// 0x00000072 System.Boolean Enemy/<Shoot>d__19::MoveNext()
extern void U3CShootU3Ed__19_MoveNext_m0EED36FB816AA227AA833AEACDF27C85912F1DB4 (void);
// 0x00000073 System.Object Enemy/<Shoot>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBBCC8D2F3CA638909BF182D2AF8F4FA0C5E06701 (void);
// 0x00000074 System.Void Enemy/<Shoot>d__19::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__19_System_Collections_IEnumerator_Reset_m807F4B8D52A48ECECF01B00B14C9C4E73C61C773 (void);
// 0x00000075 System.Object Enemy/<Shoot>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__19_System_Collections_IEnumerator_get_Current_mB27882ABF4CDE6CEBCA6021FEA20C27234B407F8 (void);
// 0x00000076 System.Void ItemDrop::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7 (void);
// 0x00000077 System.Void ItemDrop::.ctor()
extern void ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2 (void);
// 0x00000078 System.Void CallScene::Call(System.String)
extern void CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE (void);
// 0x00000079 System.Void CallScene::ReloadScene(System.String)
extern void CallScene_ReloadScene_m0DE10917B25AB783BF06FA91CC3A5E7E40F6D0F9 (void);
// 0x0000007A System.Void CallScene::.ctor()
extern void CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C (void);
// 0x0000007B System.Void DestroyTime::Start()
extern void DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497 (void);
// 0x0000007C System.Collections.IEnumerator DestroyTime::CallDestroy()
extern void DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6 (void);
// 0x0000007D System.Void DestroyTime::.ctor()
extern void DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88 (void);
// 0x0000007E System.Void DestroyTime/<CallDestroy>d__2::.ctor(System.Int32)
extern void U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF (void);
// 0x0000007F System.Void DestroyTime/<CallDestroy>d__2::System.IDisposable.Dispose()
extern void U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD (void);
// 0x00000080 System.Boolean DestroyTime/<CallDestroy>d__2::MoveNext()
extern void U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B (void);
// 0x00000081 System.Object DestroyTime/<CallDestroy>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2 (void);
// 0x00000082 System.Void DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.Reset()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9 (void);
// 0x00000083 System.Object DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0 (void);
// 0x00000084 System.Void Explosion::Start()
extern void Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22 (void);
// 0x00000085 System.Collections.IEnumerator Explosion::Explode()
extern void Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258 (void);
// 0x00000086 System.Void Explosion::.ctor()
extern void Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094 (void);
// 0x00000087 System.Void Explosion/<Explode>d__3::.ctor(System.Int32)
extern void U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F (void);
// 0x00000088 System.Void Explosion/<Explode>d__3::System.IDisposable.Dispose()
extern void U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8 (void);
// 0x00000089 System.Boolean Explosion/<Explode>d__3::MoveNext()
extern void U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC (void);
// 0x0000008A System.Object Explosion/<Explode>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02 (void);
// 0x0000008B System.Void Explosion/<Explode>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7 (void);
// 0x0000008C System.Object Explosion/<Explode>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB (void);
// 0x0000008D System.Void FollowTransform::LateUpdate()
extern void FollowTransform_LateUpdate_m13C18C78CA1ED04CA0A466DAC6C69E572F872694 (void);
// 0x0000008E System.Void FollowTransform::.ctor()
extern void FollowTransform__ctor_m8C70301FD199E67FFE18894ABE784E1987237103 (void);
// 0x0000008F Stick/stck Stick::GetStck()
extern void Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B (void);
// 0x00000090 DataHandler DataHandler::get_Instance()
extern void DataHandler_get_Instance_m074EEE6B52961382B624BA1040EFA52CEDD43127 (void);
// 0x00000091 System.Void DataHandler::Awake()
extern void DataHandler_Awake_mB2F28A0A88C287B3027588CB75B2B3C105D6FC3B (void);
// 0x00000092 System.Void DataHandler::InitializeData()
extern void DataHandler_InitializeData_m31BDF811059FF36590F0A382D22CA104ACBDAE5B (void);
// 0x00000093 System.Void DataHandler::LoadDefaultData()
extern void DataHandler_LoadDefaultData_m5E7BFE11EF80078207123F7B337BFD976FDDF834 (void);
// 0x00000094 System.Void DataHandler::.ctor()
extern void DataHandler__ctor_mF18869F217CF7F40EB76794A8C4C77B52CA1173E (void);
// 0x00000095 System.Void ControlShip::Start()
extern void ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8 (void);
// 0x00000096 System.Void ControlShip::LateUpdate()
extern void ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D (void);
// 0x00000097 System.Void ControlShip::AnimateMotor()
extern void ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27 (void);
// 0x00000098 System.Collections.IEnumerator ControlShip::Shoot()
extern void ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA (void);
// 0x00000099 System.Void ControlShip::CallShield()
extern void ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C (void);
// 0x0000009A System.Void ControlShip::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73 (void);
// 0x0000009B System.Void ControlShip::.ctor()
extern void ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93 (void);
// 0x0000009C System.Void ControlShip/<Shoot>d__16::.ctor(System.Int32)
extern void U3CShootU3Ed__16__ctor_m34916B429E472DCCAB8545976B05E21A0F82EDD1 (void);
// 0x0000009D System.Void ControlShip/<Shoot>d__16::System.IDisposable.Dispose()
extern void U3CShootU3Ed__16_System_IDisposable_Dispose_mDEEB2EF12BA67674E13E07F9B1D0A2FCA2F23FE5 (void);
// 0x0000009E System.Boolean ControlShip/<Shoot>d__16::MoveNext()
extern void U3CShootU3Ed__16_MoveNext_mD63FDF8822EEB1B3960FBB0C45E03A3692DC0486 (void);
// 0x0000009F System.Object ControlShip/<Shoot>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m05E497F0401ABEAA6DC2D4014177198C08363B8F (void);
// 0x000000A0 System.Void ControlShip/<Shoot>d__16::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__16_System_Collections_IEnumerator_Reset_m2945F9E62AACBFC5E77244CA23A9197100CFAAA0 (void);
// 0x000000A1 System.Object ControlShip/<Shoot>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__16_System_Collections_IEnumerator_get_Current_mD9960268E8871B02290A54F2F1F7D1B12963E76D (void);
// 0x000000A2 System.Void ShipGhost::Update()
extern void ShipGhost_Update_m9CBFA67917456099B47A651AD8EF61C9DEDFCFED (void);
// 0x000000A3 System.Void ShipGhost::.ctor()
extern void ShipGhost__ctor_m0C2EC522D241D2F57436393ED62BA9C8A04E1EC6 (void);
// 0x000000A4 System.Void ShipUI::UpdateShield(System.Single,System.Single)
extern void ShipUI_UpdateShield_m2ADAF2ABD1A87818FA282E517BA22A6671EF6A21 (void);
// 0x000000A5 System.Void ShipUI::.ctor()
extern void ShipUI__ctor_m2D3D2BCD7DE49BB92D508580B7BE9103DD83906F (void);
// 0x000000A6 System.Int32 FindJob.SaveSystem.SaveData::get_CurrentStage()
extern void SaveData_get_CurrentStage_m63CBCB3DA521F9179C47795F827A95717538A746 (void);
// 0x000000A7 System.Void FindJob.SaveSystem.SaveData::set_CurrentStage(System.Int32)
extern void SaveData_set_CurrentStage_m1157E33ED9ECD2BFBB0106D73A3E642159821E6D (void);
// 0x000000A8 System.Void FindJob.SaveSystem.SaveData::.ctor(System.Int32)
extern void SaveData__ctor_mA1576E2B62CF5A2D3ED543EFA0BF2925906384ED (void);
// 0x000000A9 System.Void FindJob.SaveSystem.SaveSystem::SaveData(FindJob.SaveSystem.SaveData)
extern void SaveSystem_SaveData_mA4DED348A09FFD90C2FC7C69B31C5D71D252F9DE (void);
// 0x000000AA FindJob.SaveSystem.SaveData FindJob.SaveSystem.SaveSystem::LoadData()
extern void SaveSystem_LoadData_mFB9B97161421FD11ABE5C5304796FEC749931848 (void);
// 0x000000AB System.Void FindJob.SaveSystem.SaveSystem::.ctor()
extern void SaveSystem__ctor_mB09307B6339D301ECF772E537A0F2F0B908D1F52 (void);
// 0x000000AC System.Void FindJob.SaveSystem.SaveSystem::.cctor()
extern void SaveSystem__cctor_m84FF17BF52B6C08FEDB2ADCF95258B2F06917FA3 (void);
// 0x000000AD System.Void Flux.ReadMe::.ctor()
extern void ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56 (void);
// 0x000000AE System.Void Gaminho.Level::.ctor()
extern void Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9 (void);
// 0x000000AF System.Void Gaminho.ScenarioLimits::.ctor()
extern void ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B (void);
// 0x000000B0 System.Void Gaminho.Shot::.ctor()
extern void Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3 (void);
// 0x000000B1 UnityEngine.Quaternion Gaminho.Statics::FaceObject(UnityEngine.Vector2,UnityEngine.Vector2,Gaminho.Statics/FacingDirection)
extern void Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50 (void);
// 0x000000B2 System.Void Gaminho.Statics::.cctor()
extern void Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4 (void);
static Il2CppMethodPointer s_methodPointers[178] = 
{
	AudioController_get_Instance_mED6F11E1732F5F8C02D9F001DB4F47011C1332DB,
	AudioController_Awake_m46F0037C36EE66B9987DE7C7FEE05248EC66457D,
	AudioController_SetMasterVolume_m24FFD1948D78C3019C36D7547693FBE25F3D24B4,
	AudioController__ctor_mFBC6DD9163DE0AC6DB9CD48A5D56326117FCDDEA,
	Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD,
	Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178,
	Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926,
	ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225,
	ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50,
	ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534,
	ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13,
	ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074,
	ControlGame_PauseLogic_m563DA59562EABB99F810ADC81B48820DF75F5969,
	ControlGame_PauseGame_m341859C0FD7778EBDA18E51802B2BDBACA7F8F16,
	ControlGame_ResumeGame_mB7970A4D517A15D3AF2ABEB768BFB365B20D3A46,
	ControlGame_Quit_m4E86B42237B8B159E22DB886C98D4728917951BD,
	ControlGame_HireDev_mC60F47B42C315084299E00B0F57ADD0755A250B0,
	ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D,
	ControlGame__cctor_m6C5D0F35594B1A5759E17D9EB7EC4B947A1B9E1F,
	ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51,
	ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB,
	ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747,
	ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA,
	Dodge_Start_mD051BAAE091535AD3F0D41FAB28C371524FF35C1,
	Dodge_Update_m8AFBBB092AA88131C1CF4FD90951AA6583C388FB,
	Dodge_DangerChecker_mA729042296D9D642677101BFC792F3AE58F94FF3,
	Dodge_PerformDodge_m7A57CE5FD4F7D7BE417163909D44BCD1361F73D7,
	Dodge_OnDrawGizmos_mA816507E0A3F53D71A0BFBD2E301FF08A996F6DB,
	Dodge__ctor_mF2B67DAAE4FDA0BDC7962837BE577374DA570E52,
	EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F,
	EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B,
	EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E,
	EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101,
	EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF,
	U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15,
	U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2,
	U3CProcessU3Ed__4_MoveNext_m3A08B74C2B6D96C327D949D051A8EA8D5B423DDC,
	U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996,
	U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363,
	U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9,
	U3CCallBossU3Ed__6_MoveNext_mDF3544E995D32004659624A65FB35CCD76C0B909,
	U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9,
	U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D,
	U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27,
	RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F,
	RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC,
	RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4,
	Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8,
	Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315,
	Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14,
	Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6,
	Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3,
	Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285,
	Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F,
	U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146,
	U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A,
	U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C,
	U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7,
	U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE,
	U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6,
	U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62,
	U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25,
	Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C,
	Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6,
	Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420,
	Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006,
	Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F,
	Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029,
	Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306,
	U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962,
	U3CShowPartsU3Ed__5_MoveNext_mCD5696204CDF2A5ECBC56EF51E055A070AC9FB77,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF,
	U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9,
	U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521,
	U3CAttackNowU3Ed__6_MoveNext_mEA7C3F8A7C4FA8E6D4387CE9F9A6709AFEB394E7,
	U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA,
	U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A,
	U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0,
	U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327,
	U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF,
	U3CAttackU3Ed__7_MoveNext_mA361D488087872EB4D071C00B7A2E97B943ED301,
	U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52,
	U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7,
	U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F,
	Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02,
	Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2,
	Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12,
	Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8,
	Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5,
	Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759,
	Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767,
	Enemy_DangerChecker_mDEDE63E42CFF2B4849A499F531D806C8FDEA16DA,
	Enemy_PerformDodge_mD2926F96DBB18BAE7F936108FAC924BD05028BE8,
	Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA,
	Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	U3CKillMeU3Ed__17__ctor_m3DA8A933BD3CC129D87F3A47FBD8C14D2E95590D,
	U3CKillMeU3Ed__17_System_IDisposable_Dispose_mB779954E5AB9233392E43E900B2E83A2F6FF32E9,
	U3CKillMeU3Ed__17_MoveNext_mCFD48D9A2CFE2952982C67DCF50FF9A171C10F7A,
	U3CKillMeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m807A3FCAA01770682E10BDF7E62BB914840C8DA1,
	U3CKillMeU3Ed__17_System_Collections_IEnumerator_Reset_m08CD3B59C1BE9755AE2142FCE7754A444ABEE665,
	U3CKillMeU3Ed__17_System_Collections_IEnumerator_get_Current_m6BE8FFE908ABBD42DAD6B9AC5198ACC1D874495E,
	U3CShootU3Ed__19__ctor_m98BA24A73038966BD3A9243D46291DA0F80E7127,
	U3CShootU3Ed__19_System_IDisposable_Dispose_m896974EDA174BA23BC280639447BAB578440E27A,
	U3CShootU3Ed__19_MoveNext_m0EED36FB816AA227AA833AEACDF27C85912F1DB4,
	U3CShootU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBBCC8D2F3CA638909BF182D2AF8F4FA0C5E06701,
	U3CShootU3Ed__19_System_Collections_IEnumerator_Reset_m807F4B8D52A48ECECF01B00B14C9C4E73C61C773,
	U3CShootU3Ed__19_System_Collections_IEnumerator_get_Current_mB27882ABF4CDE6CEBCA6021FEA20C27234B407F8,
	ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7,
	ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2,
	CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE,
	CallScene_ReloadScene_m0DE10917B25AB783BF06FA91CC3A5E7E40F6D0F9,
	CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C,
	DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497,
	DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6,
	DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88,
	U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF,
	U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD,
	U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B,
	U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0,
	Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22,
	Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258,
	Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094,
	U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F,
	U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8,
	U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC,
	U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB,
	FollowTransform_LateUpdate_m13C18C78CA1ED04CA0A466DAC6C69E572F872694,
	FollowTransform__ctor_m8C70301FD199E67FFE18894ABE784E1987237103,
	Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B,
	DataHandler_get_Instance_m074EEE6B52961382B624BA1040EFA52CEDD43127,
	DataHandler_Awake_mB2F28A0A88C287B3027588CB75B2B3C105D6FC3B,
	DataHandler_InitializeData_m31BDF811059FF36590F0A382D22CA104ACBDAE5B,
	DataHandler_LoadDefaultData_m5E7BFE11EF80078207123F7B337BFD976FDDF834,
	DataHandler__ctor_mF18869F217CF7F40EB76794A8C4C77B52CA1173E,
	ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8,
	ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D,
	ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27,
	ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA,
	ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C,
	ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73,
	ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93,
	U3CShootU3Ed__16__ctor_m34916B429E472DCCAB8545976B05E21A0F82EDD1,
	U3CShootU3Ed__16_System_IDisposable_Dispose_mDEEB2EF12BA67674E13E07F9B1D0A2FCA2F23FE5,
	U3CShootU3Ed__16_MoveNext_mD63FDF8822EEB1B3960FBB0C45E03A3692DC0486,
	U3CShootU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m05E497F0401ABEAA6DC2D4014177198C08363B8F,
	U3CShootU3Ed__16_System_Collections_IEnumerator_Reset_m2945F9E62AACBFC5E77244CA23A9197100CFAAA0,
	U3CShootU3Ed__16_System_Collections_IEnumerator_get_Current_mD9960268E8871B02290A54F2F1F7D1B12963E76D,
	ShipGhost_Update_m9CBFA67917456099B47A651AD8EF61C9DEDFCFED,
	ShipGhost__ctor_m0C2EC522D241D2F57436393ED62BA9C8A04E1EC6,
	ShipUI_UpdateShield_m2ADAF2ABD1A87818FA282E517BA22A6671EF6A21,
	ShipUI__ctor_m2D3D2BCD7DE49BB92D508580B7BE9103DD83906F,
	SaveData_get_CurrentStage_m63CBCB3DA521F9179C47795F827A95717538A746,
	SaveData_set_CurrentStage_m1157E33ED9ECD2BFBB0106D73A3E642159821E6D,
	SaveData__ctor_mA1576E2B62CF5A2D3ED543EFA0BF2925906384ED,
	SaveSystem_SaveData_mA4DED348A09FFD90C2FC7C69B31C5D71D252F9DE,
	SaveSystem_LoadData_mFB9B97161421FD11ABE5C5304796FEC749931848,
	SaveSystem__ctor_mB09307B6339D301ECF772E537A0F2F0B908D1F52,
	SaveSystem__cctor_m84FF17BF52B6C08FEDB2ADCF95258B2F06917FA3,
	ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56,
	Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9,
	ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B,
	Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3,
	Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50,
	Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4,
};
static const int32_t s_InvokerIndices[178] = 
{
	2531,
	1606,
	1397,
	1606,
	1362,
	1373,
	1606,
	1606,
	1606,
	1606,
	1606,
	1606,
	1606,
	1606,
	1606,
	1606,
	1606,
	1606,
	2546,
	1606,
	1606,
	1606,
	1606,
	1606,
	1606,
	1606,
	1410,
	1606,
	1606,
	1606,
	1565,
	1606,
	1565,
	1606,
	1362,
	1606,
	1588,
	1565,
	1606,
	1565,
	1362,
	1606,
	1588,
	1565,
	1606,
	1565,
	1606,
	1606,
	1606,
	1606,
	1565,
	1606,
	1565,
	1565,
	1373,
	1606,
	1362,
	1606,
	1588,
	1565,
	1606,
	1565,
	1362,
	1606,
	1588,
	1565,
	1606,
	1565,
	1606,
	1109,
	1565,
	1108,
	1565,
	1373,
	1606,
	1362,
	1606,
	1588,
	1565,
	1606,
	1565,
	1362,
	1606,
	1588,
	1565,
	1606,
	1565,
	1362,
	1606,
	1588,
	1565,
	1606,
	1565,
	1606,
	1606,
	1373,
	1606,
	1565,
	1362,
	1565,
	1606,
	1410,
	1606,
	1606,
	1606,
	1362,
	1606,
	1588,
	1565,
	1606,
	1565,
	1362,
	1606,
	1588,
	1565,
	1606,
	1565,
	1373,
	1606,
	1373,
	1373,
	1606,
	1606,
	1565,
	1606,
	1362,
	1606,
	1588,
	1565,
	1606,
	1565,
	1606,
	1565,
	1606,
	1362,
	1606,
	1588,
	1565,
	1606,
	1565,
	1606,
	1606,
	2525,
	2531,
	1606,
	1606,
	1606,
	1606,
	1606,
	1606,
	1606,
	1565,
	1606,
	1373,
	1606,
	1362,
	1606,
	1588,
	1565,
	1606,
	1565,
	1606,
	1606,
	894,
	1606,
	1553,
	1362,
	1362,
	2505,
	2531,
	1606,
	2546,
	1606,
	1606,
	1606,
	1606,
	2050,
	2546,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	178,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
