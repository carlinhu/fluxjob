using UnityEngine;

namespace FindJob.SaveSystem
{
    [System.Serializable]
    public class SaveData
    {
        private int currentStage;
        public int CurrentStage { get { return currentStage; } set { currentStage = value; } }

        public SaveData(int _currentStage)
        {
            currentStage = _currentStage;
        }
    }
}

