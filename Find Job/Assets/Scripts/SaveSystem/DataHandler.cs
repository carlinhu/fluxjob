using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;
using FindJob.SaveSystem;

public class DataHandler : MonoBehaviour
{
    #region Singleton
    private static DataHandler _Instance;
    public static DataHandler Instance
    {
        get
        {
            if (_Instance == null)
            {
                Debug.LogError("AudioController instance not found.");
                return null;
            }
            return _Instance;
        }
    }
    #endregion

    private void Awake()
    {
        _Instance = this;
        DontDestroyOnLoad(this);
    }

    public SaveData saveData;

    public void InitializeData()
    {
        SaveData _saveData = SaveSystem.LoadData();
        if (_saveData != null)
        {
            saveData = _saveData;
            Statics.CurrentLevel = saveData.CurrentStage;
        }
        else
            LoadDefaultData(); //If the system couldn't find saved data, it loads the default data.
    }

    public void LoadDefaultData()
    {
        //Reset the variables to start the game from scratch
        Statics.WithShield = false;
        Statics.EnemiesDead = 0;
        Statics.CurrentLevel = 0;
        Statics.Points = 0;
        Statics.ShootingSelected = 2;

        saveData = new SaveData(Statics.CurrentLevel); //Creates a new instance of Save Data to save the info.
    }
}
