using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace FindJob.SaveSystem
{
    public class SaveSystem : MonoBehaviour
    {
        public static string defaultFilePath = Application.persistentDataPath;
        public static string fileName = "/SaveData.flux";

        public static void SaveData(SaveData data)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = defaultFilePath + "/" + fileName;
            Debug.Log($"Data saved at: {path}");
            FileStream stream = new FileStream(path, FileMode.Create);

            SaveData saveData = new SaveData(data.CurrentStage);

            formatter.Serialize(stream, saveData);
            stream.Close();
        }

        public static SaveData LoadData()
        {
            string path = defaultFilePath + fileName;
            if (File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);

                SaveData saveData = formatter.Deserialize(stream) as SaveData;
                stream.Close();
                Debug.Log($"Data loaded from: {path}, Current Stage: {saveData.CurrentStage + 1}");
                return saveData;
            }
            else
            {
                Debug.Log("Save file not found, loading default settings.");
                return null;
            }
        }
    }
}