using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipGhost : MonoBehaviour
{
    public GameObject ship;
    [SerializeField] private Vector3 positionOffset;
    [SerializeField] private float ghostDelayFactor;
    private float timeStep;

    void Update()
    {
        timeStep += Time.deltaTime;

        if(timeStep >= ghostDelayFactor)
        {
            Vector3 finalPos = ship.transform.position + positionOffset;
            transform.position = Vector3.Lerp(transform.position, finalPos, Time.deltaTime * ghostDelayFactor);
            transform.rotation = Quaternion.Slerp(transform.rotation, ship.transform.rotation, Time.deltaTime * ghostDelayFactor);
        }
    }
}
