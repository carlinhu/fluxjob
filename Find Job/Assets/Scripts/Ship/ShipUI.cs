using UnityEngine;
using UnityEngine.UI;

public class ShipUI : MonoBehaviour
{
    public Image shieldImageFill;

    public void UpdateShield(float _max, float _value)
    {
        shieldImageFill.fillAmount = _value / _max;
    }
}
