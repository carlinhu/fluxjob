using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTransform : MonoBehaviour
{
    public Transform target;
    public Vector3 offSet;
    public Vector3 rotation;

    void LateUpdate()
    {
        transform.position = target.position + offSet;
        transform.rotation = Quaternion.Euler(rotation);
    }
}
