using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour
{
    #region Singleton
    private static AudioController _Instance;
    public static AudioController Instance
    {
        get
        {
            if (_Instance == null)
            {
                Debug.LogError("AudioController instance not found.");
                return null;
            }
            return _Instance;
        }
    }
    #endregion

    private void Awake()
    {
        _Instance = this;
    }

    public AudioMixer masterMixer;

    public void SetMasterVolume(float _volume)
    {
        _volume = Mathf.Clamp(_volume, 0.0001f, 1f);
        masterMixer.SetFloat("MasterVolume", Mathf.Log10(_volume) * 20f);
    }
}
