using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dodge : MonoBehaviour
{
    private Rigidbody2D rb;
    public float dangerZoneSize = 300f;
    private bool isDodging;
    private float dodgeTimer;
    public float defDodgeTimer = 0.5f;

    private void Start()
    {
        dodgeTimer = defDodgeTimer;
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        DangerChecker();
    }

    private void DangerChecker()
    {
        if (dodgeTimer >= 0f)
            dodgeTimer -= Time.deltaTime;
        else
            isDodging = false;
        
        Collider2D danger = Physics2D.OverlapCircle(transform.position, dangerZoneSize);
        if (!danger.CompareTag("Shot") || isDodging)
            return;

        Rigidbody2D dangerRb = danger.GetComponent<Rigidbody2D>();

        if(dangerRb != null)
        {
            Vector2 dangerVelocit = dangerRb.velocity.normalized;
            Vector2 dodgeDirection = Vector2.Perpendicular(dangerVelocit);
            PerformDodge(dodgeDirection);
            Debug.DrawRay(transform.position, dodgeDirection * 150f, Color.red, 2f);
        }
    }

    private void PerformDodge(Vector2 direction)
    {
        isDodging = true;
        dodgeTimer = defDodgeTimer;

        rb.velocity = Vector3.zero;
        rb.AddForce((300f * rb.mass) * direction, ForceMode2D.Impulse);
    }

    public void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, dangerZoneSize);
    }
}
