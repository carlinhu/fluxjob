﻿using Gaminho;
using UnityEngine;
using UnityEngine.UI;

public class ControlStart : MonoBehaviour {
    public Text Record;

	// Use this for initialization
	void Start () {
        //dataHandler = GetComponent<DataHandler>();
        //dataHandler.InitializeData();//Initialize the game data

        //Loads Record
        if (PlayerPrefs.GetInt(Statics.PLAYERPREF_VALUE) == 0)
        {
            PlayerPrefs.SetString(Statics.PLAYERPREF_NEWRECORD, "Nobody");
        }
        Record.text = "Record: " + PlayerPrefs.GetString(Statics.PLAYERPREF_NEWRECORD) + "(" + PlayerPrefs.GetInt(Statics.PLAYERPREF_VALUE) + ")";     
	}

    public void StartClick()
    {
        /*#if !UNITY_EDITOR
            Debug.Log("Σφάλμα σκόπιμα, το βρήκατε, συγχαρητήρια!");
            Sair();
            return;
        #endif*/
        DataHandler.Instance.LoadDefaultData();
        GetComponent<AudioSource>().Stop();
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_HISTORY) as GameObject);
    }

    public void Quit()
    {
        Application.Quit();
    }

    
}
