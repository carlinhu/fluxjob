﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;
using System;

public class Enemy : MonoBehaviour
{

    public Statics.TYPE_ENEMY MyType;
    public Life _life;
    public GameObject Explosion;
    public int Damage = 1;
    public Shot shot;
    public GameObject ItemDrop;
    private Rigidbody2D rb;

    [Header("Dodge")]
    [SerializeField] private bool dodgeEnable;
    [SerializeField] private float dangerZoneSize = 300f;
    [SerializeField] private float defDodgeTimer = 0.5f;
    [SerializeField] private List<String> dangerTags = new List<string>();
    private bool isDodging;
    private float dodgeTimer;


    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if (MyType == Statics.TYPE_ENEMY.SHIP)
        {
            StartCoroutine(Shoot());

            if (!dodgeEnable)
                return;
            dodgeTimer = defDodgeTimer;
            DangerChecker();
        }
    }

    private void Update()
    {
        if (!Statics.Player) return;
        if (MyType == Statics.TYPE_ENEMY.SHIP)
        {
            Quaternion q = Statics.FaceObject(Statics.Player.localPosition, transform.localPosition, Statics.FacingDirection.Right);
            transform.rotation = q;
        }
    }



    private void OnCollisionEnter2D(Collision2D objeto)
    {
        if (objeto.gameObject.tag == "Shot")
        {
            Destroy(objeto.gameObject);
            _life.TakesLife(Statics.Damage);
            Instantiate(Explosion, transform).transform.parent = transform.parent;
        }
    }


    public void MyDeath()
    {
        //Explosao;
        if (MyType == Statics.TYPE_ENEMY.METEOR)
        {
            if (UnityEngine.Random.Range(0, 100) > 60)//60% chance of it becoming bits and pieces
            {
                Create(1);
                Create(2);
                Create(3);
                Create(4);
            }
            Statics.EnemiesDead++;

        }
        if (ItemDrop != null && UnityEngine.Random.Range(0, 100) > 70)
        {
            Instantiate(ItemDrop, transform.parent).transform.localPosition = transform.localPosition;
        }
        Statics.Points += Damage * 100;
        StartCoroutine(KillMe());
    }

    private IEnumerator KillMe()
    {
        yield return new WaitForSeconds(0.1f);
        Destroy(gameObject);
    }

    private void Create(int v)
    {

        GameObject goMunus = Instantiate(gameObject, transform.parent);
        goMunus.GetComponent<Enemy>().MyType = Statics.TYPE_ENEMY.PIECES;
        float scale = UnityEngine.Random.Range(0.2f, 0.6f);
        goMunus.transform.localScale = new Vector3(scale, scale, scale);
        float force = 100f * goMunus.GetComponent<Rigidbody2D>().mass;
        switch (v)
        {
            case 1:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.down * force, ForceMode2D.Impulse);
                break;
            case 2:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * force, ForceMode2D.Impulse);
                break;
            case 3:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.left * force, ForceMode2D.Impulse);
                break;
            case 4:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * force, ForceMode2D.Impulse);
                break;
        }
    }

    private IEnumerator Shoot()
    {
        //Pode executar até 3 tiros simultaneos
        while (true)
        {
            yield return new WaitForSeconds(shot.ShootingPeriod);

            Statics.Damage = shot.Damage;
            GameObject goShooter = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
            goShooter.transform.parent = transform;
            goShooter.transform.localPosition = shot.Weapon.transform.localPosition;
            goShooter.GetComponent<Rigidbody2D>().AddForce(transform.up * ((shot.SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
            goShooter.AddComponent<BoxCollider2D>();
            goShooter.transform.parent = transform.parent;
            goShooter.tag = "EnemyShot";

            if (shot.TypeShooter == Statics.TYPE_SHOT.DOUBLE)
            {
                GameObject goShooter2 = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                goShooter2.transform.parent = transform;
                goShooter2.transform.localPosition = shot.Weapon2.transform.localPosition;
                goShooter2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((shot.SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                goShooter2.AddComponent<BoxCollider2D>();
                goShooter2.transform.parent = transform.parent;
                goShooter2.tag = "EnemyShot";
            }

            if (shot.TypeShooter == Statics.TYPE_SHOT.TRIPLE)
            {
                GameObject goShooter2 = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                goShooter2.transform.parent = transform;
                goShooter2.transform.localPosition = shot.Weapon2.transform.localPosition;
                goShooter2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((shot.SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                goShooter2.AddComponent<BoxCollider2D>();
                goShooter2.transform.parent = transform.parent;
                goShooter2.tag = "EnemyShot";

                GameObject goShooter3 = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                goShooter3.transform.parent = transform;
                goShooter3.transform.localPosition = shot.Weapon3.transform.localPosition;
                goShooter3.GetComponent<Rigidbody2D>().AddForce(transform.up * ((shot.SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                goShooter3.AddComponent<BoxCollider2D>();
                goShooter3.transform.parent = transform.parent;
                goShooter3.tag = "EnemyShot";
            }



        }
    }

    #region Dodge Ability
    private void DangerChecker()
    {
        if (dodgeTimer >= 0f)
            dodgeTimer -= Time.deltaTime;
        else
            isDodging = false;

        Collider2D danger = Physics2D.OverlapCircle(transform.position, dangerZoneSize);
        if (!dangerTags.Contains(danger.gameObject.tag) || isDodging)
            return;

        Rigidbody2D dangerRb = danger.GetComponent<Rigidbody2D>();

        if (dangerRb != null)
        {
            Vector2 dangerVelocit = dangerRb.velocity.normalized;
            Vector2 dodgeDirection = Vector2.Perpendicular(dangerVelocit);
            PerformDodge(dodgeDirection);
            Debug.DrawRay(transform.position, dodgeDirection * 300f, Color.red, 2f);
        }
    }

    private void PerformDodge(Vector2 direction)
    {
        isDodging = true;
        dodgeTimer = defDodgeTimer;

        rb.velocity = Vector3.zero;
        rb.AddForce((300f * rb.mass) * direction, ForceMode2D.Impulse);
    }
    #endregion

    public void EndBoss()
    {
        GameObject.Find("Control").GetComponent<ControlGame>().LevelPassed();
        Destroy(gameObject);
        Invoke("PStick", 1f);
    }

    void PStick()
    {
        if (Stick.GetStck() == Stick.stck.MANESTIC)
        {
            Update();
        }
    }
}
